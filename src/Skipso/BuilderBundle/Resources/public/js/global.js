//
var objCount = 0;
var currentStyle = '';

//
$("#Objects").accordion({});

//
(function (old) {
    $.fn.attr = function () {
        if (arguments.length === 0) {
            if (this.length === 0) {
                return null;
            }

            var obj = {};
            $.each(this[0].attributes, function () {
                if (this.specified) {
                    obj[this.name] = this.value;
                }
            });
            return obj;
        }

        return old.apply(this, arguments);
    };
})($.fn.attr);

//
var keys = {}, settings = {};


function object_setting(objID) {

    $('#form-'+objID).modal('show');
    $('#form-'+objID+' .modal-content').draggable({containment: $('#myModal'), handle: ".modal-header"});
}

$(document).keydown(function (e) {
    keys[e.which] = true;
    checkKeys()
});

$(document).keyup(function (e) {
    delete keys[e.which];
});

function checkKeys() {
    if ((keys.hasOwnProperty(224) || keys.hasOwnProperty(17)) && keys.hasOwnProperty(79)) {
        keys = {};
        $('#templateFiles').click();
    }
}

// only for debug
function LOG(txt) {
    return;
    console.log(txt);
}

//
function open_popup(url) {
    var w = 880;
    var h = 570;
    var l = Math.floor((screen.width - w) / 2);
    var t = Math.floor((screen.height - h) / 2);
    var win = window.open(url, 'ResponsiveFilemanager', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
}

//
function object_tools(objID) {
    //return ('');
    //
    var str = '<div class="object_handler">';
    str += '<span class="glyphicon glyphicon-th"></span>';
    str += ' <span class="glyphicon glyphicon-cog" onclick="object_setting(\'' + objID + '\')"></span>';
    str += ' <span class="glyphicon glyphicon-remove" onclick="if(confirm(\'are you sure ?\')){$(\'#' + objID + '\').remove()}"></span>';
    str += '</div>';
    return str;
}

function object_mouse_over(obj) {
    //return;
    $(obj).find('.object_handler').show();
}

function object_mouse_out(obj) {
    //return;
    $(obj).find('.object_handler').hide();
}

function deleteMedia(hash)
{
    $.ajax(
        {url: removeMediafile +'/'+hash,
        success: function(data){
            $('tr#image-'+hash).remove();
        }}
    )
}
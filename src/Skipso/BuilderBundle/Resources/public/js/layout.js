function fixHelper(e, ui) {
    var $ctr = $(this);
    ui.helper
        .addClass('btn')
        .outerWidth($ctr.outerWidth())
        .find('.mx-content-hover')
        .removeClass('mx-content-hover')
        .end();
}
function toggleHover(e) {
    if (e.type == 'mouseenter')
        $(this).addClass('mx-content-hover');
    else
        $(this).removeClass('mx-content-hover');
}
function dropRow(element, objID) {
    console.log($(element));
    var index = $('#template-body').children().index($(element)), column = 0, rowhash = 0, extractID = objID.split('-'), apicall, newrow = false;
    if (extractID.length == 1) {
        apicall = createWidget + '/' + objID;
        newrow = true;
    }
    else apicall = updateRowOrder + '/' + extractID[1];

    $.ajax({
        url: apicall,
        type: 'POST',
        dataType: 'json',
        data: {index: index},
        success: function (data) {
            if (newrow) {
                $(element).after(data.preview);
                $(element).remove();
                $(".aColumn").sortable({
                    handle: ".glyphicon-th",
                    connectWith: "#template-body .aColumn",
                    stop: function (e, ui) {
                        //console.log(ui.target);
                        setTimeout(function () {
                            dropWidget(ui.item, ui.item.attr('id'))
                        }, 200)
                    }
                });
            }
            CallBackLayout(data.id);
        }
    })
}
function dropWidget(element, objID) {
    $column = $($(element).parents('.aColumn')[0]);
    $row = $($(element).parents('.line')[0]);
    var rowhash = $row.attr('id').split('-')[1];
    var index = $column.children().index($(element)), column = $row.find('.layout_row').children().index($column), extractID = objID.split('-'), apicall, newrow = false;
    if (extractID.length == 1) {
        apicall = createWidget + '/' + objID;
        newrow = true;
    }
    else {
        apicall = updateWidgetOrder + '/' + rowhash + '/' + extractID[1];
    }
    $.ajax({
        url: apicall,
        type: 'POST',
        dataType: 'json',
        data: {index: index, column: column, rowhash: rowhash},
        success: function (data) {
            if (newrow) {
                $(element).after(data.preview);
                $(element).remove();
            }
            CallBackLayout(data.id);
        }
    })
}
function removeWidget(url) {
    $.ajax({
        url: url,
        success: function (data) {
            $('#' + data.id).remove()
        }
    })
}

function showTab(element) {
    $(element).tab('show');
}
sdCfg = {
    cursor: 'move',
    zIndex: 200,
    opacity: 0.75,
    scroll: false,
    appendTo: 'body',
    helper: 'clone',
    start: fixHelper
};
$('.rows li').draggable({
    cursor: 'move',
    zIndex: 200,
    opacity: 0.75,
    cursorAt: {top: 0, left: 0},
    scroll: false,
    appendTo: 'body',
    helper: function (e, ui) {
        return '<div class="widget" id="' + $(this).attr('id') + '"><div class="layout_row"></div></div>';
    },
    connectToSortable: '#template-body',
    start: fixHelper,
    stop: function (e, ui) {
        $preElement = $('#template-body').find('li.ui-draggable-handle');
        if ($preElement.length)
            dropRow($preElement, $(this).attr('id'));
    }
}).hover(toggleHover);
$('.widgets li').draggable({
    cursor: 'move',
    zIndex: 200,
    opacity: 0.75,
    cursorAt: {top: 0, left: 0},
    scroll: false,
    appendTo: 'body',
    helper: function (e, ui) {
        return '<div class="widget" id="' + $(this).attr('id') + '"><div class="layout_row"></div></div>';
    },
    connectToSortable: '#template-body .aColumn',
    start: fixHelper,
    stop: function (e, ui) {
        $preElement = $('#template-body').find('li.ui-draggable-handle');
        if ($preElement.length)
            dropWidget($preElement, $(this).attr('id'));
    }
}).hover(toggleHover);
//$('#template-body .line, #template-body .widget').draggable({
//    cursor: 'move',
//    zIndex: 200,
//    opacity: 0.75,
//    scroll: false,
//    appendTo: 'body',
//    helper: 'clone',
//    stop: function(e, ui){
//        console.log(ui.draggable);
//        //$preElement = $('#template-body').find('li');
//        //dropObject($preElement, ui.helper.attr('rel'));
//    }
//})
$('#template-body').sortable({
    containment: 'parent',
    tolerance: 'pointer',
    handle: ".glyphicon-th",
    helper: 'clone',
    start: fixHelper,
    update: function (e, ui) {
        //console.log(this);
        //console.log(ui.item)
        //ui.item.draggable(sdCfg)
        //    .hover( toggleHover );
        //$(this).sortable('option', 'containment', 'parent');
    },
    stop: function (e, ui) {
        if (typeof ui.item.attr('id') != 'undefined')
            setTimeout(function () {
                dropRow(ui.item, ui.item.attr('id'))
            }, 200)
        //updateWidget(ui.item.attr('id'))
    }
})
$("#template-body .aColumn").sortable({
    connectWith: "#template-body .aColumn",
    tolerance: 'pointer',
    handle: ".glyphicon-th",
    helper: 'clone',
    update: function (e, ui) {
        //console.log(this);
        //console.log(ui.item)
        //ui.item.draggable(sdCfg)
        //    .hover( toggleHover );
        //$(this).sortable('option', 'containment', 'parent');
    },
    stop: function (e, ui) {
        if (typeof ui.item.attr('id') != 'undefined')
            setTimeout(function () {
                dropWidget(ui.item, ui.item.attr('id'))
            }, 200)
    }
})


function updateOrders($element) {
    var index = $element.parents('.aColumn').find('.widget').index($element);
    console.log(index);
    var rowID = $element.parents('.line').attr('id').split('-')[1];
}
function CallBackLayout(objID) {
    //
    $('#' + objID + ' a').click(function () {
        return false;
    })

    $('#' + objID).mouseover(function () {
        $(this).find('.settings').show();
        $(this).addClass('layout-hover');
    }).mouseout(function () {
        $(this).find('.settings').hide();
        $(this).removeClass('layout-hover');
    });
}

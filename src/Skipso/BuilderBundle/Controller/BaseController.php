<?php

namespace Skipso\BuilderBundle\Controller;

use Skipso\BuilderBundle\Entity\Mediafile;
use Skipso\BuilderBundle\Entity\Row;
use Skipso\BuilderBundle\Entity\Widget;
use Skipso\BuilderBundle\Form\MediafileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Skipso\BuilderBundle\Helper\WidgetFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/builder")
 */
class BaseController extends Controller
{
    /**
     * @Route("/index")
     * @Template()
     */
    public function indexAction()
    {
        $pageBuilder = $this->getPageBuilder();
        $pagePreview = "";

//        var_dump($pageBuilder->getStack());exit;

        foreach ($pageBuilder->getStack() as $widget) {
//            var_dump($widget->getId());
            $pagePreview .= $this->renderWidget($widget);
        }
//        exit('here');
        return array('preview' => $pagePreview);
    }

    /**
     * @Route("/save.{_format}", name="builder_save", defaults={"_format"="json"})
     */
    public function saveAction()
    {
        $pageBuilder = $this->getPageBuilder();
        $stack = $pageBuilder->getStack();
        $em = $this->getDoctrine()->getManager();
//        $em->getConnection()->beginTransaction();
        try {
            foreach ($stack as $row) {
                $em->persist($row);
            }
            $em->flush();
//            $em->getConnection()->commit();

//            exit('comes here');
            $pageBuilder->saveStack($stack);
        } catch (\Exception $e) {
//            $em->getConnection()->rollback();
//            $em->close();
            $json = new JsonResponse(array('error' => $e->getMessage()));
            $json->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $json;
        }
        return new JsonResponse(array('status' => true));
    }

    /**
     * @Route("/create/{instance}.{_format}", name="builder_create_widget", defaults={"_format"="json","instance"="rowone"})
     * @Method({"POST"})
     */
    public function createWidgetAction(Request $request, $instance)
    {
        $widget = WidgetFactory::initWidget($instance);
        $index = $request->get('index');
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($widget);
//        $em->flush();
        $builder = $this->getPageBuilder();
        if ($widget instanceof Row)
            $builder->newRowAtIndex($widget, (int)$index);
        else if ($widget instanceof Widget) {
            $rowHash = $request->get('rowhash');
            $column = $request->get('column');
            $row = $builder->getObjectByHash($rowHash);
//            var_dump($row->getHash(), $column, $index);exit;
            $row->addWidgetByOrder($widget, $column, $index);

            $builder->updateRowAtIndex($row);
        }
        $instanceClass = WidgetFactory::getInstanceClass($instance);
        $preview = $this->renderView("SkipsoBuilderBundle:" . $instanceClass . ':preview.html.twig', array('widget' => $widget, 'time' => time()));
        return new JsonResponse(array('hash' => $widget->getHash(), 'id' => $widget->getName() . '-' . $widget->getHash(), 'preview' => $preview));
    }

    /**
     * @Route("/updatewidgetorder/{rowHash}/{widgetHash}.{_format}", name="builder_updatewidget_order", defaults={"_format"="json","rowHash"="0","widgetHash"="0"})
     */
    public function updateWidgetOrderAction(Request $request, $widgetHash, $rowHash)
    {
        $pageBuilder = $this->getPageBuilder();
        $column = $request->get('column');
        $index = $request->get('index');
        $pageBuilder->updateWidgetByHash($widgetHash, $rowHash, $column, $index);
        return new JsonResponse(array('status' => true));
    }

    /**
     * @Route("/updateroworder/{rowHash}.{_format}", name="builder_updaterow_order", defaults={"_format"="json","rowHash"="0"})
     */
    public function updateRowOrderAction(Request $request, $rowHash)
    {
        $pageBuilder = $this->getPageBuilder();
        $row = $pageBuilder->getObjectByHash($rowHash);
        $pageBuilder->updateRowAtIndex($row, (int)$request->get('index'));
        return new JsonResponse(array('status' => true));
    }


    /**
     * @Route("/removewidget/{hash}.{_format}", name="builder_remove_widget", defaults={"_format"="json","hash"="0"})
     */
    public function removeWidgetAction($hash)
    {
        $pageBuilder = $this->getPageBuilder();
        $widget = $pageBuilder->getObjectByHash($hash);
        if ($widget instanceof Row) {//remove row
            $pageBuilder->removeRow($widget);
        } else {//remove widget and update row's children
            $row = $widget->getParent();
            $row->removeWidgetByHash($widget->getHash());
            if ($widget->getId()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($widget);
                $em->flush();
            }
            $pageBuilder->updateRowAtIndex($row);
        }
        return new JsonResponse(array('status' => true, 'id' => $widget->getName() . '-' . $widget->getHash()));
    }

    /**
     * @Route("/savesetting/{hash}.{_format}", name="builder_setting_widget", defaults={"_format"="json","hash"="0"})
     * @Method({"POST"})
     */
    public function saveSettingsAction(Request $request, $hash)
    {
        $builder = $this->getPageBuilder();

        $widget = $builder->getObjectByHash($hash);
        $form = $this->createForm(WidgetFactory::initWidgetForm($widget->getName()), $widget);
//        var_dump($widget->getContent());
        $form->submit($request);
        if ($form->isValid()) {
            $row = $widget->getParent();
            $row->updateWidget($widget);
            $builder->updateRowAtIndex($row);
            $instanceClass = WidgetFactory::getInstanceClass($widget->getName());
            $preview = $this->renderView("SkipsoBuilderBundle:" . $instanceClass . ':preview.html.twig', array('widget' => $widget, 'time' => time()));
            return new JsonResponse(array('status' => true, 'preview' => $preview, 'id' => $widget->getName() . '-' . $widget->getHash()));
        } else {
            $jsonResponse = new JsonResponse(array('error' => $form->getErrorsAsString()));
            $jsonResponse->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $jsonResponse;
        }
    }

    protected function renderWidget(Widget $widget = null, $view = 'preview')
    {
        if (is_null($widget))
            return '';
        $instanceClass = WidgetFactory::getInstanceClass($widget->getName());

        $preview = $this->renderView("SkipsoBuilderBundle:" . $instanceClass . ':' . $view . '.html.twig', array('widget' => $widget, 'time' => time()));
        return $preview;
    }

    /**
     * @return \Skipso\BuilderBundle\Services\PageBuilder
     */
    protected function getPageBuilder()
    {
        return $this->get('skipso.page.builder');
    }

    /**
     * @Route("/carousel/upload/{hash}.{_format}", name="builder_carousel_upload", defaults={"_format"="json"})
     */
    public function uploadCarouselAction(Request $request, $hash)
    {
        $builder = $this->getPageBuilder();
        $widget = $builder->getObjectByHash($hash);
        $media = new Mediafile();
        $form = $this->createForm(new MediafileType(), $media);
        $form->submit($request);
        if ($form->isValid()) {
            $widget->addMedia($media);
            $row = $widget->getParent();
            $row->updateWidget($widget);
            $builder->updateRowAtIndex($row);
            $instanceClass = WidgetFactory::getInstanceClass($widget->getName());
            $cacheManager = $this->get('liip_imagine.cache.manager');
            $srcPath = $cacheManager->getBrowserPath($media->getWebPath(), 'images_list');
            $preview = $this->renderView("SkipsoBuilderBundle:" . $instanceClass . ':preview.html.twig', array('widget' => $widget, 'time' => time()));
            return new JsonResponse(array('status' => true, 'src' => $srcPath, 'imageHash' => $media->getHash(), 'preview' => $preview, 'id' => $widget->getName() . '-' . $widget->getHash()));
        }
        $json = new JsonResponse(array('status' => false, 'error' => $form->getErrorsAsString()));
        $json->setStatusCode(Response::HTTP_BAD_REQUEST);
        return $json;
    }

    /**
     * @Route("/carousel/removemedia/{hash}.{_format}", name="builder_carousel_removemedia", defaults={"_format"="json","hash"="0"})
     */
    public function removeMediaAction($hash)
    {
        $builder = $this->getPageBuilder();
        $items = $builder->removeMediaByHash($hash);
        if (is_array($items)) {
            $row = $items['row'];
            $media = $items['media'];
            $em = $this->getDoctrine()->getManager();
            if ($media->getId() && $row->getId()) {
                $em->persist($row);
                $em->flush();
            }
        }
        return new JsonResponse(array('status' => true));
    }

}

<?php

namespace Skipso\BuilderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Skipso\BuilderBundle\Helper\WidgetFactory;

class DefaultController extends BaseController
{

    /**
     * @Route("/render/widget/{widgetHash}/{view}")
     */
    public function renderWidgetAction($widgetHash, $view)
    {
        $pageBuilder = $this->getPageBuilder();
        $widget = $pageBuilder->getObjectByHash($widgetHash);
        return new Response($this->renderWidget($widget, $view));
    }

    /**
     * @Route("/render/setting/{hash}")
     */
    public function renderSettingWidgetAction($hash)
    {
        $builder = $this->getPageBuilder();
        $widget = $builder->getObjectByHash($hash);

        $instanceClass = WidgetFactory::getInstanceClass($widget->getName());

        $form = $this->createForm(WidgetFactory::initWidgetForm($widget->getName()), $widget);
        $formView = $this->renderView("SkipsoBuilderBundle:" . $instanceClass . ':form.html.twig', array('form' => $form->createView(), 'widget' => $widget, 'hash' => $hash, 'time' => $hash.time()));

        return new Response($formView);
    }

    /**
     * @Route("/preview", name="builder_preview")
     * @Template()
     */
    public function previewAction()
    {
        $pageBuilder = $this->getPageBuilder();
        $pagePreview = "";

        foreach ($pageBuilder->getStack() as $widget) {
            $pagePreview .= $this->renderWidget($widget, 'view');
        }
        return array('preview' => $pagePreview);

    }
}

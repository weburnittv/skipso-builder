<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/23/14
 * Time: 1:57 AM
 */

namespace Skipso\BuilderBundle\Services;

use Skipso\BuilderBundle\Entity\CarouselWidget;
use Skipso\BuilderBundle\Entity\Row;
use Skipso\BuilderBundle\Entity\Widget;
use Doctrine\ORM\EntityManager;

class PageBuilder
{

    private $session;
    private $stack;
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct($session, $em)
    {
        $this->session = $session;
        $this->em = $em;
    }

    public function getStack()
    {
        if (is_array($this->stack))
            return $this->stack;

        if ($this->session)
            $stack = $this->session->fetch('page_builder');
        else $stack = array();

        if (is_array($stack) && !empty($stack)) {
            foreach ($stack as $key => $row) {
                if (is_object($row) && $row->getId())
                    $stack[$key] = $this->em->merge($row);
            }
            $this->stack = $stack;
            return $stack;
        } else return array();
    }

    /**
     * @todo Move widget to new row(hash) according to $column and $index
     * @param $widgetHash
     * @param $rowHash
     * @param $column
     * @param $index
     */
    public function updateWidgetByHash($widgetHash, $rowHash, $column, $colIndex)
    {
        $widget = null;
        $oldRow = null;
        $newRow = null;

        $stack = $this->getStack();
        foreach ($stack as $index => $row) {
            if ($row->getHash() == $rowHash) {
                $newRow = $row;
            }
            foreach ($row->getChildren() as $wg)
                if ($wg->getHash() == $widgetHash) {
                    $widget = $wg;
                    $oldRow = $row;
//                    var_dump($oldRow->getOrder(), $widget->getHash());exit;
                    break;
                }
        }
        if (is_null($widget) | is_null($oldRow) | is_null($newRow))
            return;

        $oldRow->removeWidgetByHash($widget->getHash());
        $stack[$oldRow->getOrder()] = $oldRow;

        $newRow->addWidgetByOrder($widget, (int)$column, (int)$colIndex);
        $stack[$newRow->getOrder()] = $newRow;
        $this->saveStack($stack);
    }

    /**
     * @todo At new row at index
     * @param Row $row
     * @param int $index
     */
    public function newRowAtIndex(Row &$row, $index = 0)
    {
        $stack = $this->getStack();
        $row->setOrder($index);
        if (!empty($stack)) {
            $tmp = null;
//            var_dump($stack);
            foreach ($stack as $key => $value) {
                if ((int)$key == (int)$index) {
                    $tmp = $value;
                    $stack[$key] = $row;//move row to $index
                } else if ((int)$index < (int)$key && $tmp) {
                    $tmp->setOrder((int)$key);
                    $stack[(int)$key] = $tmp;//lift old row($tmp)
                    $tmp = $value;
                }
            }
            if ($tmp) {
                $tmp->setOrder(count($stack));
                $stack[count($stack)] = $tmp;
            } else {
                $row->setOrder((int)$index);
                $stack[(int)$index] = $row;
            }
        } else $stack[$index] = $row;
        $this->saveStack($stack);
    }

    /**
     * @todo update new index path for specific row
     * @param Row $row
     * @param int $index
     */
    public function updateRowAtIndex(Row &$row, $index = null)
    {
        $stack = $this->getStack();
        $foundBeforeUpdate = false;
        $foundIndex = false;
        if ($index)
            $row->setOrder($index);

        if (!empty($stack)) {
            $tmp = null;
            foreach ($stack as $key => $value) {
                if ($value->getHash() == $row->getHash()) {
                    if (is_null($index)) {
                        $stack[$key] = $row;
                        break;
                    }
                    $foundBeforeUpdate = true;
                    $value->setOrder($index);
                    $row = $value;
                    $stack[$key] = $row;
                }
                if ((int)$key === $index) {
                    $foundIndex = true;
                }

                if (!$foundBeforeUpdate && $foundIndex)//move up
                {
                    if (!$tmp) {
                        $tmp = $stack[(int)$key];
                    } else {
                        $nextItem = $stack[(int)$key];
                        $tmp->setOrder((int)$key);
                        $stack[(int)$key] = $tmp;
                        $tmp = $nextItem;
                    }
                } else if ($foundBeforeUpdate && !$foundIndex) {//move down
                    $nextItem = $stack[(int)$key + 1];
                    $nextItem->setOrder((int)$key);
                    $stack[(int)$key] = $nextItem;//lift up item
                } else if ($foundBeforeUpdate && $foundIndex) {
                    $tmp->setOrder((int)$key);
                    $stack[$key] = $tmp;
                    $stack[$index] = $row;
                    break;
                }
            }
        }
        $this->saveStack($stack);
        return $this;
    }

    public function saveWidgetSettings(Widget $newWidget)
    {
//        var_dump($newWidget->getContent(), $newWidget->getId(), $newWidget->getHash());
        foreach ($this->getStack() as $column => $row) {

//            var_dump($newWidget->getContent(), $newWidget->getId(), $newWidget->getHash());
//            exit('entire for loop');
            if ($row->getHash() == $newWidget->getHash()) {
                $stack[$column] = $newWidget;
                break;
            } else {
                foreach ($row->getChildren() as $wg) {
                    if ($newWidget->getHash() == $wg->getHash()) {
                        $row->removeChild($wg);
                        $row->addChild($newWidget);
                        $stack[$column] = $row;
                        break;
                    }
                }
            }
        }
        $this->saveStack($stack);
        return $this;
    }

    /**
     * @param $hash
     * @return Widget
     */
    public function getObjectByHash($hash)
    {
        $stack = $this->getStack();
        foreach ($stack as $row) {
            if ($hash == $row->getHash())
                return $row;
            else
                foreach ($row->getChildren() as $widget) {
                    if ($widget->getHash() == $hash)
                        return $widget;
                }
        }
        return;
    }

    public function removeRow(Row $row)
    {
        $stack = $this->getStack();
        $foundBeforeUpdate = false;
        if (!empty($stack)) {
            $tmp = null;
            foreach ($stack as $key => $value) {
                if ($value->getHash() == $row->getHash()) {
                    $foundBeforeUpdate = true;
                    $nextItem = $stack[$key + 1];
                    $nextItem->setOrder((int)$key);
                    $stack[$key] = $nextItem;
                } else if ($foundBeforeUpdate) {
                    if (isset($stack[$key + 1])) {
                        $nextItem = $stack[$key + 1];
                        $nextItem->setOrder((int)$key);
                        $stack[$key] = $nextItem;
                    } else {
                        unset($stack[$key]);
                    }
                }
            }
        }
        $this->saveStack($stack);
        return $this;
    }

    public function removeMediaByHash($hash)
    {
        $stack = $this->getStack();
        foreach ($stack as $key => $row) {
            foreach ($row->getChildren() as $widget)
                if ($widget instanceof CarouselWidget) {
                    foreach ($widget->getMedias() as $media) {
                        if ($media->getHash() == $hash) {
                            $widget->removeMedia($media);
                            $stack[$key] = $row;
                            $this->saveStack($stack);
                            return array('row' => $row, 'media' => $media);
                        }
                    }
                }
        }
    }

    public function saveStack($stack)
    {
        $this->stack = array();
        $this->stack = $stack;

        if (is_object($this->session)) {
            foreach ($this->stack as $key => $row) {
                if ($row->getId()) {
                    $this->em->detach($row);
                    $this->stack[$key] = $row;
                }
            }
            $this->session->save('page_builder', $this->stack, 3600);
//            $this->session->save();
//            var_dump($stack);exit;
        }

        return $this;
    }

    public function getCurrentStack()
    {
        return $this->stack;
    }
}
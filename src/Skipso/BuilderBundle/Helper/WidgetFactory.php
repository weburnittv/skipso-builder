<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/21/14
 * Time: 1:28 PM
 */
namespace Skipso\BuilderBundle\Helper;

class WidgetFactory {

    /**
     * @param $instance
     * @return \Skipso\BuilderBundle\Entity\Widget
     */
    public static function initWidget($instance)
    {
        if(substr($instance, 0, 3) == 'row')
            return new \Skipso\BuilderBundle\Entity\Row($instance);
        else {
            $widget = '\\Skipso\\BuilderBundle\\Entity\\'.ucfirst($instance).'Widget';
            return new $widget($instance);
        }
    }

    /**
     * @param $instance
     * @return \Skipso\BuilderBundle\Entity\Widget
     */
    public static function initWidgetForm($instance)
    {
        if(substr($instance, 0, 3) == 'row')
            return new \Skipso\BuilderBundle\Form\WidgetType();
        else {
            $type = '\\Skipso\\BuilderBundle\\Form\\'.ucfirst($instance).'FormType';
            return new $type();
        }
    }

    /**
     * @param $instance
     * @return \Skipso\BuilderBundle\Entity\Widget
     */
    public static function getInstanceClass($instance)
    {
        if(substr($instance, 0, 3) == 'row')
            return "Row";
        else {
            return ucfirst($instance)."Widget";
        }
    }
}
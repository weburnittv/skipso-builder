<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/21/14
 * Time: 2:07 PM
 */

namespace Skipso\BuilderBundle\Helper;


class StringUtil {

    public static function generateReadableRandomString($length = 12) {
        $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}
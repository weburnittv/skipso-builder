<?php

namespace Skipso\BuilderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WidgetType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('margin', new SpacingType())
            ->add('padding', new SpacingType())
            ->add('color')
            ->add('backgroundColor')
            ->add('border', new BorderType())
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Skipso\BuilderBundle\Entity\Widget'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'widget';
    }
}

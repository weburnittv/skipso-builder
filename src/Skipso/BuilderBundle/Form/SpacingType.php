<?php

namespace Skipso\BuilderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SpacingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('top', 'text', array('required' => false))
            ->add('left', 'text', array('required' => false))
            ->add('right', 'text', array('required' => false))
            ->add('bottom', 'text', array('required' => false));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Skipso\BuilderBundle\Entity\Spacing'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'spacing';
    }
}

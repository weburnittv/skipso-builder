<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/21/14
 * Time: 2:52 PM
 */

namespace Skipso\BuilderBundle\Form;

use Proxies\__CG__\Skipso\BuilderBundle\Entity\CarouselWidget;
use Symfony\Component\Form\FormBuilderInterface;

class CarouselFormType extends WidgetType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'choice', array('choices' => CarouselWidget::getTypeOptions()));
        parent::buildForm($builder, $options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'carousel';
    }
}
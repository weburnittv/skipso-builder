<?php

namespace Skipso\BuilderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BorderType extends SpacingType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('color', 'text', array('required' => false))
            ->add('type', 'choice', array('required' => false, 'choices' => array('solid' => 'solid', 'dashed' => 'dashed', 'dotted' => 'dotted', 'double' => 'double', 'groove' => 'groove', 'inset' => 'inset', 'outset' => 'outset', 'ridge' => 'ridge')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Skipso\BuilderBundle\Entity\Border'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'border';
    }
}

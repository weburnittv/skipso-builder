<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/21/14
 * Time: 2:52 PM
 */

namespace Skipso\BuilderBundle\Form;

use Proxies\__CG__\Skipso\BuilderBundle\Entity\CarouselWidget;
use Symfony\Component\Form\FormBuilderInterface;

class ParagraphFormType extends WidgetType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content', 'textarea', array('required' => true));
        parent::buildForm($builder, $options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'paragraph';
    }
}
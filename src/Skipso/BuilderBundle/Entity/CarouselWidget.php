<?php

namespace Skipso\BuilderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarouselWidget
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CarouselWidget extends Widget implements WidgetInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="Mediafile", mappedBy="widget")
     */
    protected $medias;

    const CAROUSEL_TYPE_RANDOM = 'random';

    const CAROUSEL_TYPE_SLIT = 'slit';

    const CAROUSEL_TYPE_BLUEPRINT = 'blueprint';

    const CAROUSEL_TYPE_FADEIN = 'fade';

//    const CAROUSEL_TYPE_RANDOM = 1;
//
//    const CAROUSEL_TYPE_RANDOM = 1;
//
//    const CAROUSEL_TYPE_RANDOM = 1;
//
//    const CAROUSEL_TYPE_RANDOM = 1;

    public function __construct()
    {
        $this->type = self::CAROUSEL_TYPE_BLUEPRINT;
        parent::__construct();
    }

    /**
     * Set type
     *
     * @param string $type
     * @return CarouselWidget
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string js file path for this widget
     */
    public function getScripts()
    {
        // TODO: Implement getScripts() method.
    }

    /**
     * @return string css file path for this widget
     */
    public function getStyles()
    {
        // TODO: Implement getStyles() method.
    }

    /**
     * @return string widget's name
     */
    public function getName()
    {
        return 'carousel';
    }

    public static function getTypeOptions(){
        return array(self::CAROUSEL_TYPE_RANDOM => 'Random', self::CAROUSEL_TYPE_BLUEPRINT => 'Blueprint', self::CAROUSEL_TYPE_SLIT => 'Slit');
    }
}

<?php

namespace Skipso\BuilderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Row
 *
 * @ORM\MappedSuperclass
 * @ORM\Table(name="Row")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Row extends Widget
{

    /**
     * @var integer
     *
     * @ORM\Column(name="col", type="smallint")
     */
    protected $col;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderIndex", type="smallint")
     */
    protected $order;

    /**
     * Refer to arrangement for children
     * @var array
     *
     * @ORM\Column(name="arrange", type="array", nullable=true)
     */
    protected $arrange;

    const ROW_ONE_COLUMN = 'rowone';
    const ROW_TWO_COLUMN = 'rowtwo';
    const ROW_THREE_COLUMN = 'rowthree';

    public function __construct($instance = null)
    {
        $this->arrange = array();
        $instances = array(self::ROW_ONE_COLUMN => 1, self::ROW_TWO_COLUMN => 2, self::ROW_THREE_COLUMN => 3);
        if ($instance && isset($instances[$instance]))
            $this->col = $instances[$instance];
        else $this->col = 1;
        parent::__construct($instance);
    }


    /**
     * Set col
     *
     * @param integer $col
     * @return Row
     */
    public function setCol($col)
    {
        $this->col = $col;

        return $this;
    }

    /**
     * Get col
     *
     * @return integer
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * @return string widget's name
     */
    public function getName()
    {
        $instances = array(1 => self::ROW_ONE_COLUMN, 2 => self::ROW_TWO_COLUMN, 3 => self::ROW_THREE_COLUMN);
        return $instances[$this->col];
    }

    /**
     * @todo Add Widget at index
     * @param Widget $widget
     * @param int $column
     * @param int $index
     */
    public function addWidgetByOrder(Widget $widget, $column = 0, $index = 0)
    {
        $this->addChild($widget);
        if (!is_array($this->arrange) || empty($this->arrange)) {
            $this->arrange = array();
            $this->arrange[$column][$index] = $widget->getHash();
        } else {
            ksort($this->arrange);
            if (!isset($this->arrange[$column][$index]))
                $this->arrange[$column][$index] = $widget->getHash();
            else foreach ($this->arrange as $col => $value) {
                if ($col == $column) {
                    $tmp = '';
                    foreach ($value as $row => $hash) {
                        if ((int)$row == (int)$index) {
                            $tmp = $hash;
                            $this->arrange[$col][$row] = $widget->getHash();
                        } else if ($tmp) {
                            $this->arrange[$col][$row] = $tmp;
                            $tmp = $hash;
                        }
                    }
                    if ($tmp)
                        $this->arrange[$col][count($value)] = $tmp;
                }
            }
        }

    }

    public function removeWidgetByHash($widgetHash)
    {
        foreach ($this->children as $child) {
            if ($child->getHash() == $widgetHash)
                $this->children->removeElement($child);
        }
        if (is_array($this->arrange) && !empty($this->arrange)) {
            ksort($this->arrange);
            foreach ($this->arrange as $key => $value) {
                $startMoving = false;
                foreach ($value as $row => $hash) {
                    if ($hash == $widgetHash) {
                        $startMoving = true;
                    }
                    if (isset($this->arrange[$key][(int)$row + 1]) && $startMoving) $this->arrange[$key][$row] = $this->arrange[$key][(int)$row + 1];
                }
                if ($startMoving) {
                    unset($this->arrange[$key][count($value) - 1]);
                    break;
                }
            }
        }
    }

    public function getItemHashesAtColumn($col)
    {
        if (is_array($this->arrange))
            foreach ($this->arrange as $column => $value) {
                if ((int)$column == (int)$col)
                    return $value;
            }
        return array();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function setArrange($arrange)
    {
        $this->arrange = $arrange;
        return $this;
    }

    public function getArrange()
    {
        if (!is_null($this->arrange))
            return $this->arrange;
        return array();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/28/14
 * Time: 2:38 PM
 */

namespace Skipso\BuilderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParagraphWidget
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ParagraphWidget extends Widget
{

    /**
     * @var integer
     *
     * @ORM\Column(name="content", type="string")
     */
    private $content;

    public function __construct(){
        $this->content = 'Default paragraph content';
        parent::__construct();
    }

    /**
     * @return string widget's name
     */
    public function getName()
    {
        return 'paragraph';
    }

    /**
     * @return int
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param int $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

}
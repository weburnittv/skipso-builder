<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/21/14
 * Time: 11:41 AM
 */

namespace Skipso\BuilderBundle\Entity;


interface WidgetInterface {

    /**
     * @return string js file path for this widget
     */
    public function getScripts();

    /**
     * @return string css file path for this widget
     */
    public function getStyles();

}
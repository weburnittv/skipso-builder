<?php

namespace Skipso\BuilderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Skipso\BuilderBundle\Helper\StringUtil;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Mediafile
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Mediafile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=12)
     */
    private $hash;

    /**
     * @ORM\ManyToOne(targetEntity="Widget", inversedBy="medias")
     * @ORM\JoinColumn(name="widget_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $widget;

    /**
     * @Assert\File(maxSize="1024k", mimeTypes={"image/jpeg", "image/png", "image/bmp", "image/gif"})
     */
    private $photoFile;

    public function __construct()
    {
        $this->hash = StringUtil::generateReadableRandomString(12);
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Mediafile
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Mediafile
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    public function getAbsolutePath()
    {
        $file = $this->file;
        return null === $file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath()
    {
        $file = $this->file;
        $dir = str_replace(DIRECTORY_SEPARATOR, '/', $this->getUploadDir());
        return null === $file ? '/' . $this->getUploadDir() : $dir . '/' . $file;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return getcwd() . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    public function getMediaDir()
    {
        return __DIR__ . '/../../../../web';
    }

    protected function getUploadDir()
    {
        return 'mediafile' . DIRECTORY_SEPARATOR . $this->getHash();
    }

    /**
     * @return mixed
     */
    public function getWidget()
    {
        return $this->widget;
    }

    /**
     * @param mixed $widget
     */
    public function setWidget(Widget $widget = null)
    {
        $this->widget = $widget;
        return $this;
    }

    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    public function setPhotoFile(\Symfony\Component\HttpFoundation\File\UploadedFile $photoFile)
    {
        $this->photoFile = $photoFile;
        $this->file = str_replace(" ", "_", $photoFile->getClientOriginalName());
        try {
            if (is_object($this->photoFile)) {
                $this->photoFile->move($this->getUploadDir(), $this->file);
                unset($this->photoFile);
            }
        } catch (\Exception $e) {

        }

        return $this;
    }
}

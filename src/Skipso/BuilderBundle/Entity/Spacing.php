<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/21/14
 * Time: 3:52 PM
 */

namespace Skipso\BuilderBundle\Entity;


class Spacing extends \ArrayObject
{
    public function __construct($array = null)
    {
        if(is_null($array) | empty($array))
        {
            $this['top'] = 0;
            $this['right'] = 0;
            $this['left'] = 0;
            $this['bottom'] = 0;
        } else {
            parent::__construct($array);
        }
    }

    /**
     * @param mixed $top
     */
    public function setTop($top)
    {
        $this['top'] = $top;
    }

    /**
     * @param mixed $right
     */
    public function setRight($right)
    {
        $this['right'] = $right;
    }

    /**
     * @param mixed $bottom
     */
    public function setBottom($bottom)
    {
        $this['bottom'] = $bottom;
    }

    /**
     * @param mixed $left
     */
    public function setLeft($left)
    {
        $this['left'] = $left;
    }

    public function getTop()
    {
        if (isset($this['top']))
            return $this['top'];
        else return 0;
    }

    public function getLeft()
    {
        if (isset($this['left']))
            return $this['left'];
        else return 0;
    }

    public function getRight()
    {
        if (isset($this['right']))
            return $this['right'];
        else return 0;
    }

    public function getBottom()
    {
        if (isset($this['bottom']))
            return $this['bottom'];
        else return 0;
    }

    public function toArray()
    {
        return array('top' => $this->getTop().'px', 'left' => $this->getLeft().'px', 'right' => $this->getRight().'px', 'bottom' => $this->getBottom().'px');
    }

}
<?php

namespace Skipso\BuilderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Skipso\BuilderBundle\Helper\StringUtil;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Widget
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Skipso\BuilderBundle\Entity\WidgetRepository")
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorMap({"normal"="Widget", "row"="Row", "carousel"="CarouselWidget", "pgr" = "ParagraphWidget"})
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 */
abstract class Widget
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var array
     *
     * @ORM\Column(name="margin", type="json_array", nullable=true)
     */
    protected $margin;

    /**
     * @var array
     *
     * @ORM\Column(name="padding", type="json_array", nullable=true)
     */
    protected $padding;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=6, nullable=true)
     */
    protected $color;

    /**
     * @var string
     *
     * @ORM\Column(name="backgroundColor", type="string", length=6, nullable=true)
     */
    protected $backgroundColor;

    /**
     * @var string
     *
     * @ORM\Column(name="border", type="json_array", nullable=true)
     */
    protected $border;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=12)
     */
    protected $hash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var Row
     * @ORM\ManyToOne(targetEntity="Row", inversedBy="children", cascade={"all"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Widget", mappedBy="parent", cascade={"all"})
     */
    protected $children;

    /**
     * @ORM\OneToMany(targetEntity="Mediafile", mappedBy="widget", cascade={"all"})
     */
    protected $medias;

    public function __construct()
    {
        $this->children = new ArrayCollection();
//        $this->margin = array('top' => 0, 'left' => 0, 'right' => 0, 'bottom' => 0);
//        $this->padding = array('top' => 0, 'left' => 0, 'right' => 0, 'bottom' => 0);
        $this->updatedAt = new \DateTime('now');
        $this->createdAt = new \DateTime('now');
        $this->hash = StringUtil::generateReadableRandomString(12);
        $this->medias = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set margin
     *
     * @param array $margin
     * @return Widget
     */
    public function setMargin($margin)
    {
        $this->margin = $margin;

        return $this;
    }

    /**
     * Get margin
     *
     * @return Spacing
     */
    public function getMargin()
    {
        return new Spacing($this->margin);
    }

    /**
     * Set padding
     *
     * @param array $padding
     * @return Widget
     */
    public function setPadding($padding)
    {
        $this->padding = $padding;

        return $this;
    }

    /**
     * Get padding
     *
     * @return Spacing
     */
    public function getPadding()
    {
        return new Spacing($this->padding);
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Widget
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set backgroundColor
     *
     * @param string $backgroundColor
     * @return Widget
     */
    public function setBackgroundColor($backgroundColor)
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    /**
     * Get backgroundColor
     *
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Widget
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Widget
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Widget
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string widget's name
     */
    public abstract function getName();

    /**
     * Set border
     *
     * @param array $border
     * @return Widget
     */
    public function setBorder($border)
    {
        $this->border = $border;

        return $this;
    }

    /**
     * Get border
     *
     * @return Spacing
     */
    public function getBorder()
    {
        return new Border($this->border);
    }

    /**
     * Set parent
     *
     * @param \Skipso\BuilderBundle\Entity\Row $parent
     * @return Widget
     */
    public function setParent(\Skipso\BuilderBundle\Entity\Row $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Skipso\BuilderBundle\Entity\Row
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \Skipso\BuilderBundle\Entity\Widget $children
     * @return Widget
     */
    public function addChild(\Skipso\BuilderBundle\Entity\Widget $children)
    {
        $children->setParent($this);
        $this->children->add($children);

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Skipso\BuilderBundle\Entity\Widget $children
     */
    public function removeChild(\Skipso\BuilderBundle\Entity\Widget $children)
    {
        $this->children->removeElement($children);
        return $this;
    }

    public function updateWidget(Widget $child)
    {
        $this->children = $this->children->map(function($kid)use($child){
            if($kid->getHash() == $child)
                return $child;
            else return $kid;
        });
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add media
     *
     * @param \Skipso\BuilderBundle\Entity\Widget $children
     * @return Widget
     */
    public function addMedia(\Skipso\BuilderBundle\Entity\Mediafile $children)
    {
        $this->medias[] = $children;
        $children->setWidget($this);

        return $this;
    }

    /**
     * Remove
     *
     * @param \Skipso\BuilderBundle\Entity\Widget $children
     */
    public function removeMedia(\Skipso\BuilderBundle\Entity\Mediafile $children)
    {
        $this->medias->removeElement($children);
        $children->setWidget(null);
        return $this;
    }

    /**
     * Get medias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedias()
    {
        return $this->medias;
    }

    public function getMarginStyle()
    {
        $margin = $this->getMargin();
        $top = $margin['top'];
        if(preg_match('/auto/', $top))
            return $top;
        else return implode(' ', $margin->toArray());
    }

    public function getPaddingStyle()
    {
        $padding = $this->getPadding();
        $top = $padding['top'];
        if(preg_match('/auto/', $top))
            return $top;
        else return implode(' ', $padding->toArray());
    }

    public function getBorderStyle()
    {
        $border = $this->getBorder()->toArray();
        $borders = array('border-top:'. $border['top'],'border-right:'. $border['right'],'border-bottom:'. $border['bottom'],'border-left:'. $border['left']);
        return implode(';', $borders);
    }
}

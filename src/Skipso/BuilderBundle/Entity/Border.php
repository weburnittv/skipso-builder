<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/28/14
 * Time: 2:15 PM
 */

namespace Skipso\BuilderBundle\Entity;


class Border extends Spacing
{

    public function setColor($color)
    {
        $this['color'] = $color;
        return $this;
    }

    public function getColor()
    {
        if (isset($this['color']))
            return $this['color'];
        else return '#fff';
    }

    public function setType($color)
    {
        $this['type'] = $color;
        return $this;
    }

    public function getType()
    {
        if (isset($this['type']))
            return $this['type'];
        else return 'solid';
    }

    public function toArray()
    {
        $tail = 'px '.$this->getType().' '.$this->getColor();
        return array('top' => $this->getTop().$tail, 'left' => $this->getLeft().$tail, 'right' => $this->getRight().$tail, 'bottom' => $this->getBottom().$tail);
    }
}
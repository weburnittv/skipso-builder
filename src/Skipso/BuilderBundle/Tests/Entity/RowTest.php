<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/23/14
 * Time: 1:24 AM
 */
namespace Skipso\BuilderBundle\Tests\Entity;

use Skipso\BuilderBundle\Entity\CarouselWidget;
use Skipso\BuilderBundle\Entity\Row;

class RowTest extends \PHPUnit_Framework_TestCase{

    public function testRemoveWidgetAtIndex()
    {
        $newRow = new Row(Row::ROW_TWO_COLUMN);
        $newCarousel = new CarouselWidget();
        $secondCarousel = new CarouselWidget();
        $newRow->addWidgetByOrder($newCarousel, 0, 0);
        $newRow->addWidgetByOrder($secondCarousel, 1, 0);
//        $newRow->setArrange(array(0 => array(0 => '12345', 1 => '23456'), 1 => array(0 => '34567', 1 => '45678')));
        $widget = new CarouselWidget();
        $newRow->addWidgetByOrder($widget, 1, 0);
        $newRow->removeWidgetByHash($secondCarousel->getHash());
        $rowArrange = $newRow->getArrange();
        $this->assertEquals(2, count($rowArrange));
    }

    public function testAddWidgetByOrder()
    {
        $newRow = new Row(Row::ROW_TWO_COLUMN);
        $newCarousel = new CarouselWidget();
        $secondCarousel = new CarouselWidget();
        $newRow->addWidgetByOrder($newCarousel, 0, 0);
        $newRow->addWidgetByOrder($secondCarousel, 1, 0);
//        $newRow->setArrange(array(0 => array(0 => '12345', 1 => '23456'), 1 => array(0 => '34567', 1 => '45678')));
        $widget = new CarouselWidget();
        $newRow->addWidgetByOrder($widget, 1, 0);
        $rowArrange = $newRow->getArrange();
        $this->assertEquals($secondCarousel->getHash(), $rowArrange[1][1]);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 12/23/14
 * Time: 1:30 PM
 */
namespace Skipso\BuilderBundle\Tests\Services;
use Skipso\BuilderBundle\Entity\CarouselWidget;
use Skipso\BuilderBundle\Entity\ParagraphWidget;
use Skipso\BuilderBundle\Entity\Row;
use Skipso\BuilderBundle\Services\PageBuilder;
use Symfony\Component\DomCrawler\Crawler;

class PageBuilderTest extends \PHPUnit_Framework_TestCase{

    public function testUpdateRowAtIndex()
    {
        $builder = new PageBuilder(null);
        $newRow = new Row();
        $builder->newRowAtIndex($newRow, 0);
        $secondRow = new Row();
        $builder->newRowAtIndex($secondRow, 1);
        $threeRow = new Row();
        $fourRow = new Row();
        $builder->newRowAtIndex($threeRow, 2);
        $builder->newRowAtIndex($fourRow, 3);

        $builder->updateRowAtIndex($fourRow, 2);

        $stack = $builder->getCurrentStack();
//        var_dump($newRow->getHash().'-'.$secondRow->getHash().'-'.$threeRow->getHash().'-'.$fourRow->getHash());
//        var_dump($stack);exit;
        $this->assertEquals($fourRow->getHash(), $stack[2]->getHash());
        $this->assertEquals($newRow->getHash(), $stack[0]->getHash());
        $this->assertEquals($secondRow->getHash(), $stack[1]->getHash());
        $this->assertEquals($threeRow->getHash(), $stack[3]->getHash());
    }

    public function testRemoveRow()
    {
        $builder = new PageBuilder(null);
        $newRow = new Row();
        $builder->newRowAtIndex($newRow, 0);
        $secondRow = new Row();
        $builder->newRowAtIndex($secondRow, 1);
        $threeRow = new Row();
        $fourRow = new Row();
        $builder->newRowAtIndex($threeRow, 2);
        $builder->newRowAtIndex($fourRow, 3);

        $builder->updateRowAtIndex($fourRow, 2);
        $builder->removeRow($secondRow);

        $stack = $builder->getCurrentStack();
//        var_dump($newRow->getHash().'-'.$secondRow->getHash().'-'.$threeRow->getHash().'-'.$fourRow->getHash());
//        var_dump($stack);exit;
//        $this->assertEquals($fourRow->getHash(), $stack[2]->getHash());
        $this->assertEquals($newRow->getHash(), $stack[0]->getHash());
        $this->assertEquals($threeRow->getHash(), $stack[2]->getHash());
        $this->assertEquals($fourRow->getHash(), $stack[1]->getHash());
    }

    public function testParagraph()
    {
        $builder = new PageBuilder(null);
        $oneRow = new Row();
        $twoRow = new Row(Row::ROW_TWO_COLUMN);
        $builder->newRowAtIndex($oneRow, 0);
        $builder->newRowAtIndex($twoRow, 1);

        $paraOne = new ParagraphWidget();
        echo $paraOne->getHash().'----';
        $oneRow->addWidgetByOrder($paraOne, 0, 0);
        $builder->updateRowAtIndex($oneRow);

        $paraTwo = new ParagraphWidget();
        echo $paraTwo->getHash().'----';
        $twoRow->addWidgetByOrder($paraTwo, 1, 0);

        $paraTh = new ParagraphWidget();
        echo $paraTh->getHash().'----';
        $twoRow->addWidgetByOrder($paraTh, 1, 0);

        $builder->updateRowAtIndex($twoRow);

        $builder->updateWidgetByHash($paraTwo->getHash(), $oneRow->getHash(), 0,0);
//        var_dump($builder->getStack());exit;
    }

    public function testAddWidgetForRow()
    {
        $builder = new PageBuilder(null);
        $newRow = new Row();
        $builder->newRowAtIndex($newRow, 0);
        $secondRow = new Row();
        $builder->newRowAtIndex($secondRow, 1);
        $threeRow = new Row();
        $fourRow = new Row(Row::ROW_TWO_COLUMN);
        $builder->newRowAtIndex($threeRow, 2);
        $builder->newRowAtIndex($fourRow, 3);

//        $builder->updateRowAtIndex($fourRow, 2);
//
        $row = $builder->getObjectByHash($fourRow->getHash());
        $row->addWidgetByOrder(new CarouselWidget(), 1, 0);
//        $builder->updateRowAtIndex($row);

        $stack = $builder->getCurrentStack();

//        $this->assertEquals($stack[2]->getArrange()[1][0], $stack[2]->getChildren()->current()->getHash());
    }

    public function testNewRowAtIndex()
    {
        $builder = new PageBuilder(null);
        $newRow = new Row();
        $builder->newRowAtIndex($newRow, 0);
        $secondRow = new Row();
        $builder->newRowAtIndex($secondRow, 1);
        $stack = $builder->getCurrentStack();
//        var_dump(($stack));
        $this->assertEquals($secondRow->getHash(), $stack[1]->getHash());
//        $this->assertEquals($newRow->getHash(), $stack[0]->getHash());
    }

    public function testUpdateWidgetByHash()
    {
        $builder = new PageBuilder(null);
        $newRow = new Row();
        $builder->newRowAtIndex($newRow, 0);
        $secondRow = new Row();
        $builder->newRowAtIndex($secondRow, 1);
//
        $another = new Row();

        $builder->newRowAtIndex($another, 0);
        $secondRows = new Row();

        $builder->newRowAtIndex($secondRows, 1);
        $threeRow = new Row();
        $fourRow = new Row(Row::ROW_TWO_COLUMN);
        $firstWidget = new CarouselWidget();
        $secondWidget = new CarouselWidget();
        $fourRow->addWidgetByOrder($firstWidget, 0, 0);
        $fourRow->addWidgetByOrder($secondWidget, 1, 0);
        $builder->newRowAtIndex($threeRow, 2);
        $builder->newRowAtIndex($fourRow, 0);
        $builder->updateWidgetByHash($firstWidget->getHash(), $newRow->getHash(), 0, 0);
        $this->assertEquals(array_filter($fourRow->getArrange()), array_filter($builder->getCurrentStack()[0]->getArrange()));
    }
}